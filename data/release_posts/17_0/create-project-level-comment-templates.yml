---
features:
  secondary:
  - name: "Project comment templates"
    available_in: [premium, ultimate] # Include all supported tiers
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/profile/comment_templates.html#for-a-project'
    image_url: '/images/17_0/create-project-level-comment-templates.png'
    reporter: phikai
    stage: create # Prefix this file name with stage-informative-title.yml
    categories:
      - 'Code Review Workflow'
      - 'Team Planning'
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/440818'
    description: |
      Following the release of [group comment templates in GitLab 16.11](https://about.gitlab.com/releases/2024/04/18/gitlab-16-11-released/#group-comment-templates), we're bringing these to projects in GitLab 17.0.

      Across an organization, it can be helpful to have the same templated response in issues, epics, and merge requests. These responses might include standard questions that need to be answered, responses to common problems, or good structure for merge request review comments. Project-level comment templates give you an additional way to scope the availability of templates, bringing organizations more control and flexibility in sharing these across users.

      To create a comment template, go to any comment box on GitLab and select **Insert comment template > Manage project comment templates**. After you create a comment template, it's available for all project members. Select the **Insert comment template** icon while making a comment, and your saved response will be applied.

      We're really excited about this iteration of comment templates and if you have any feedback, please leave it in [issue 451520](https://gitlab.com/gitlab-org/gitlab/-/issues/451520).
