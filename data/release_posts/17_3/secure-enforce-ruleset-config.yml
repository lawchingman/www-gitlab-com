---
features:
  secondary:
  - name: "Enforce the ruleset used in SAST, IaC Scanning, and Secret Detection"
    available_in: [ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/sast/customize_rulesets.html#specify-a-remote-configuration-file'
    gitlab_com: true
    add_ons: []
    reporter: connorgilbert
    stage: application_security_testing
    categories:
    - 'SAST'
    - 'Secret Detection'
    - 'Security Policy Management'
    issue_url: 
    - 'https://gitlab.com/gitlab-org/gitlab/-/issues/414732'
    description: |
      You can customize the rules used in [SAST](https://docs.gitlab.com/ee/user/application_security/sast/customize_rulesets.html), [IaC Scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/#customize-rules), and [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/index.html#customizing-analyzer-settings) by creating a local configuration file committed in the repository or by setting a CI/CD variable to apply a shared configuration across multiple projects.

      Previously, scanners preferred the local configuration file, even if you also set a shared ruleset reference.
      This precedence order made it difficult to ensure that scans would use a known, trusted ruleset.

      Now, we've added a new CI/CD variable, `SECURE_ENABLE_LOCAL_CONFIGURATION`, to control whether local configuration files are allowed.
      It defaults to `true`, which keeps the existing behavior: local configuration files are allowed and are preferred over shared configurations.
      If you set the value to `false` when you [enforce scan execution](https://docs.gitlab.com/ee/user/application_security/#enforce-scan-execution), you can be sure that scans use your shared ruleset, or the default ruleset, even if project developers add a local configuration file.
