# Data schemas

## Team Members

Please see [`team_members/person`](./team_members/person#team-member-data-schema) for a detailed account of the person data schema.

## Product sections, stages, groups

For more information on making changes to product sections, stages, and groups. Please see the [product categories handbook page](https://handbook.gitlab.com/handbook/product/categories/#changes).

## Data flow

Various data files populate other sites, primarily the handbooks (public and internal), and the marketing site.

As of 2025-02-01, below is a diagram of the main dependencies.

A further list of files used for the handbook can be found in [the sync script](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/main/scripts/sync-data.sh?ref_type=heads#L18).

```mermaid
flowchart TD
	WD[Workday]

	subgraph YAML Files
    	subgraph team.yml Fields
        	MNF["Other fields:
            	• type
            	• locality
            	• country
            	• role
            	• picture
            	• pronouns
            	• pronunciation
            	• twitter
            	• linkedin
            	• mastodon
            	• expertise
            	• domain_expertise
            	• projects
            	• story
            	• remote_story"]
        	WDF["Workday-populated fields:
            	• slug
            	• name
            	• gitlab
            	• division
            	• departments
            	• specialty
            	• job_title
            	• reports_to
            	• public"]
    	end
    	CY[categories.yml]
    	SY[stages.yml]
    	PY[projects.yml]
    	FY[features.yml]
    	SRY[services.yml]
    	DY[domain_expertise.yml]
	end

	subgraph Websites
    	HB[Handbook Site]
    	MK[Marketing Site]
    	OC[Org Chart]
	end
    
	%% Workday connections
	WD -->|Populates| WDF
    
	%% Website connections
	WDF -->|Used by| OC
	WDF -->|Used by| HB
	MNF -->|Used by| HB
	FY -->|Used by| HB
	FY -->|Used by| MK
	SY -->|Used by| HB
	SY -->|Used by| MK
	CY -->|Used by| HB
	CY -->|Used by| MK
	PY -->|Used by| HB
	SRY -->|Used by| HB
	DY -->|Used by| HB

	%% Relationships between YAML files
	SY -->|References| CY
	CY -->|References| SY
	FY -->|References| CY
	SRY -->|References| CY
	SY -->|References| FY
	MNF -->|References| PY
	MNF -->|References| DY
```
