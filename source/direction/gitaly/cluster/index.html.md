---
layout: markdown_page
title: "Category Direction - Gitaly Cluster"
description: "Gitaly Cluster is a high availability Git based storage solution."
canonical_path: "/direction/gitaly/cluster/"
---

- TOC
{:toc}

## Gitaly Cluster

In order to support highly available Git repository storage, [Gitaly Cluster](https://docs.gitlab.com/ee/administration/gitaly/) has been released. This provides redundant storage benefits such as voted writes, read distribution, and data redundancy. For full documentation, please see the details on [Configuring Gitaly Cluster](https://docs.gitlab.com/ee/administration/gitaly/praefect.html).

To fully appreciate the use-case for Gitaly Cluster, we must first clarify the role of highly available repository storage. From the Gitaly perspective, highly available (HA) storage means that a fault-tolerant interface for repository data exists, such that the loss of a single storage node will not compromise the ability to read / write Git data. Gitaly cluster fulfills this role by providing an interface to define multiple Gitaly storage nodes, and set a replication factor for stored repositories (how many nodes each repository should be stored on). In the event of a storage node loss, read and write operations continue as before, and when the cluster is returned to full capacity, the data is re-replicated to the returning node.

What HA repository storage does not provide is improved performance. Though in some cases write performance improves (through read distribution), the general concept is that you are trading storage cost and potential performance impacts for fault tolerance.

### 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them.
-->

The one year plan for Gitaly Group is founded on three main principals discussed below. As Gitaly is one of the foundational elements of GitLab, these principals were chosen ensure that Gitaly is ready to meet future business needs.

#### Improve Repository Data Safety

In the unlikely event that there is a repository data issue necessitating in data restoration, we want to ensure that there are sufficient tools in place to allow our customers to be up and running as efficiently and painlessly as possible. To this end, the Gitaly team are actively involved in the [GitLab Disaster Recovery Working Group](https://handbook.gitlab.com/handbook/company/working-groups/disaster-recovery/) where we play a critical role in defining opportunities to improve overall repository data recovery time objectives (RTO) and recovery point objectives (RPO). We also meet regularly with customers to better understand our self-managed customer needs.

Our one year focus areas are:

- Improve the user backup experience by designing a point in time backup and restore mechanism
- Apply our learnings from our GitLab Disaster Recovery Working Group to the overall Gitaly product, to improve RTO / RPO for our self-managed customers

#### Ensure Gitaly Scales Gracefully

As our customers deploy larger installations of GitLab, they start to hit more pain points around the scalability of Gitaly and its repository management. This is especially true in light of our intent to make cloud-native deployments of Gitaly a reality, where nodes may be spun up and down more regularly.

Our goals here are:

- Provide a better user journey for interacting with very large repositories (in collaboration with the [Source Code group](https://about.gitlab.com/direction/create/source_code_management/))
- Collaborate closely with our [Tenant Scale](https://about.gitlab.com/direction/core_platform/tenant-scale/) team to keep scalable repository management at the forefront of our scalability plans

#### Improve the User Experience

We recognize that administrators of self-managed instances want to more easily configure and manage repository storage. In order to assist in both of these user journeys, we plan to do the following:

- Develop a cloud-native approach for Gitaly storage, though collaboration with our [Distribution group](https://about.gitlab.com/direction/distribution/).
- Improve our Gitaly Cluster offering to be more user friendly, and easier to manage
- Become a thought leader in the Git space, providing insights and native GitLab methodologies to perform common tasks in an easy to understand way.

### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

Over the next quarter, the Gitaly team is focused on the following areas. While this is not an exhaustive list, it does give some insight into our major focus areas.

#### Reliable and Scalable Cluster Replication

As our customers continue to grow their repositories (both in count and in size), it is critical that the underlying Gitaly services can scale appropriately to meet these demands. As a team, we have decided that the solution to several of the existing scalability issues surrounding Gitaly cluster is a paradigm shift in how we replicate Git data. As such, we have begun initial efforts in shifting to a [decentralized architecture for Gitaly Cluster](https://gitlab.com/groups/gitlab-org/-/epics/8903).

The highlights of this forward thinking approach are as follows:

- The elimination of the Gitaly PostgreSQL database (Praefect) including the removal of the existing replication queue. This means that there is no more need to work around the single point of failure of this database by using external PostgreSQL redundancy solutions.
- Strong consistency guarantees, including for object pools
- Easier management for data migrations and node management
- Elimination of the difference between Gitaly and Gitaly Cluster. The resultant implementation can simply have a single Gitaly node or multiple Gitaly nodes configured to replicate data.

#### Backups and disaster recovery

While this is not an area we own as a team, we believe that it is crucial for us to support the teams working in these areas to ensure that Git backups and data management allows for rapid and accurate restoration of Git data. The team is heavily involved in leading the [GitLab disaster recovery working group](https://about.gitlab.com/company/team/structure/working-groups/disaster-recovery/). As leaders in this working group, we're collaborating with a broad cross-functional team to help ensure our service's success long-term. As part of this effort, we're architecting a solution for [implementing write-ahead logging](https://gitlab.com/groups/gitlab-org/-/epics/8911) on GitLab.com.

#### Cloud Native Gitaly

We are in the beginning stages of investigating the [known limitations](https://gitlab.com/groups/gitlab-org/-/epics/6127#known-limitations) around Gitaly running well in Kubernetes. This is going to be an ongoing theme across the next year, but we wanted to begin by ensuring that we understood first and foremost what issues exist today. The team is taking a data driven approach involving load testing in an effort to ensure we have clearly identified functional gaps.

<!-- ### What we recently completed -->
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

### What is Not Planned Right Now
<!--  Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand -->

In order to best represent our [Transparency Value](https://about.gitlab.com/handbook/values/#transparency), it is just as important to clarify what the Gitaly team cannot prioritize currently. This does not mean that we do not recognize the need for some of these features, simply that we have a finite team.

- Better Support for Administrative User Journeys

    We want to ensure that in the future, we support user journeys such as adding,
    removing, and replacing nodes cleanly, and provide a basic administrative dashboard to monitor node health.

## Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecated near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

The version control systems market is expected to be valued at close to US$550mn in the year 2021 and is estimated to reach US$971.8md by 2027 according to [Future Market Insights](https://www.futuremarketinsights.com/reports/version-control-systems-market) which is broadly consistent with revenue estimates of GitHub ([$250mn ARR](https://www.owler.com/company/github)) and Perforce ([$130mn ARR](https://www.owler.com/company/perforce)). The opportunity for GitLab to grow with the market, and grow it's share of the version control market is significant.

Git is the market leading version control system, demonstrated by the [2018 Stack Overflow Developer Survey](https://insights.stackoverflow.com/survey/2018/#work-_-version-control) where over 88% of respondents use Git. Although there are alternatives to Git, Git remains dominant in open source software, usage by developers continues to grow, it installed by default on macOS and Linux, and the project itself continues to adapt to meet the needs of larger projects and enterprise customers who are adopting Git, like the Microsoft Windows project.

According to a [2016 Bitrise survey](https://blog.bitrise.io/state-of-app-development-2016#self-hosted) of mobile app developers, 62% of apps hosted by SaaS provider were hosted in GitHub, and 95% of apps are hosted in by a SaaS provider. These numbers provide an incomplete view of the industry, but broadly represent the large opportunity for growth in SaaS hosting on GitLab.com, and in self hosted where GitLab is already very successful.

### Key Capabilities
<!-- For this product area, these are the capabilities a best-in-class solution should provide -->

**Support large repositories**

As applications mature, the existing code base continues to grow. As such, average repository sizes are on the rise and version control systems must be able to handle these large repositories in a performant manner. Additionally, many development tasks may require version control of large files, which again, should be handled seamlessly.

**Ensure data safety**

Application code has a very high value to organizations. It is unacceptable to have a solution which does not make it easy to ensure the integrity of your data, as well as provide easy means of backing up and restoring your data should something go wrong. Ideally, these solutions should use efficient and cost effective storage to optimize your business infrastructure.

<!-- #### Roadmap -->
<!-- Key deliverables we're focusing on to build a BIC solution. List the epics by title and link to the epic in GitLab. Minimize additional description here so that the epics can remain the SSOT. -->

### Top 2 Competitive Solutions
<!-- PMs can choose to highlight a primary BIC competitor--or more, if no single clear winner in the category exists; in this section we should indicate: 1. name of competitive product, 2. links to marketing website and documentation, 3. why we view them as the primary BIC competitor -->

Important competitors are [GitHub.com](https://github.com) and [Perforce](https://perforce.com) which, in relation to Gitaly, compete with GitLab in terms of raw Git performance and support for enormous repositories respectively.

Customers and prospects evaluating GitLab (GitLab.com and self hosted) benchmark GitLab's performance against GitHub.com, including Git performance. The Git performance of GitLab.com for easily benchmarked operations like cloning, fetching and pushing, show that GitLab.com similar to GitHub.com.

Perforce competes with GitLab primarily on its ability to support enormous repositories, either from binary files or monolithic repositories with extremely large numbers of files and history. This competitive advantage comes naturally from its centralized design which means only the files immediately needed by the user are downloaded. Given sufficient support in Git for partial clone, and sufficient performance in GitLab for enormous repositories, existing customers are waiting to migrate to GitLab.

- [Git for enormous repositories](https://gitlab.com/groups/gitlab-org/-/epics/773)
