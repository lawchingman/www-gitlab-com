---
title: "GitLab Patch Release: 17.8.2, 17.7.4, 17.6.5"
categories: releases
author: Rohit Shambhuni
author_gitlab: rshambhuni
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 17.8.2, 17.7.4, 17.6.5 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2025/02/12/patch-release-gitlab-17-8-2-released/'
image_title: '/images/blogimages/security-cover-fy26.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.8.2, 17.7.4, 17.6.5 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all self-managed GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version. GitLab Dedicated customers do not need to take action.

GitLab releases fixes for vulnerabilities in patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are committed to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [A CSP-bypass XSS in merge-request page](#a-csp-bypass-xss-in-merge-request-page) | High |
| [Denial of Service due to Unbounded Symbol Creation](#denial-of-service-due-to-unbounded-symbol-creation) | Medium |
| [Exfiltrate content from private issues using Prompt Injection](#exfiltrate-content-from-private-issues-using-prompt-injection) | Medium |
| [Internal HTTP header leak via route confusion in workhorse](#internal-http-header-leak-via-route-confusion-in-workhorse) | Medium |
| [SSRF via workspaces](#ssrf-via-workspaces) | Medium |
| [Unauthorized Incident Closure and Deletion by Planner Role in GitLab](#unauthorized-incident-closure-and-deletion-by-planner-role-in-gitlab) | Medium |
| [ActionCable does not invalidate tokens after revocation](#actioncable-does-not-invalidate-tokens-after-revocation) | Medium |
| [A custom permission may allow overriding Repository settings](#a-custom-permission-may-allow-overriding-repository-settings) | Low |

### A CSP-bypass XSS in merge-request page

An XSS vulnerability exists in GitLab CE/EE affecting all versions from 13.3 prior to 17.6.5, 17.7 prior to 17.7.4 and 17.8 prior to 17.8.2 that allows an attacker to execute unauthorized actions via a change page.
This is a high severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:C/C:H/I:H/A:N), 8.7).
It is now mitigated in the latest release and is assigned [CVE-2025-0376](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0376).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.


### Denial of Service due to Unbounded Symbol Creation

A denial of service vulnerability in GitLab CE/EE affecting all versions from 14.1 prior to 17.6.5, 17.7 prior to 17.7.4, and 17.8 prior to 17.8.2 allows an attacker to impact the availability of GitLab via unbounded symbol creation via the scopes parameter in a Personal Access Token.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H), 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-12379](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-12379).

Thanks [sim4n6](https://hackerone.com/sim4n6) for reporting this vulnerability through our HackerOne bug bounty program.


### Exfiltrate content from private issues using Prompt Injection

An issue was discovered in GitLab EE affecting all versions starting from 16.0 prior to 17.6.5, starting from 17.7 prior to 17.7.4, and starting from 17.8 prior to 17.8.2, which allows an attacker to exfiltrate contents of a private issue using prompt injection.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:H/I:H/A:N), 6.4).
It is now mitigated in the latest release and is assigned [CVE-2024-3303](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-3303).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Internal HTTP header leak via route confusion in workhorse

An information disclosure vulnerability in GitLab CE/EE affecting all versions from 8.3 prior to 17.6.5, 17.7 prior to 17.7.4, and 17.8 prior to 17.8.2 allows an attacker to send a crafted request to a backend server to reveal sensitive information.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2025-1212](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-1212).

This vulnerability has been discovered internally by GitLab team member [Joern Schneeweisz](https://gitlab.com/joernchen).


### SSRF via workspaces

An external service interaction vulnerability in GitLab EE affecting all versions from 15.11 prior to 17.6.5, 17.7 prior to 17.7.4, and 17.8 prior to 17.8.2 allows an attacker to send requests from the GitLab server to unintended services.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-9870](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-9870).

Thanks [retr02332](https://hackerone.com/retr02332) for reporting this vulnerability through our HackerOne bug bounty program.


### Unauthorized Incident Closure and Deletion by Planner Role in GitLab

Improper Authorization in GitLab CE/EE affecting all versions from 17.7 prior to 17.7.4, 17.8 prior to 17.8.2 allow users with limited permissions to perform unauthorized actions on critical project data.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/#vector=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2025-0516](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-0516).

Thanks [sp4rrow](https://hackerone.com/sp4rrow) for reporting this vulnerability through our HackerOne bug bounty program.


### ActionCable does not invalidate tokens after revocation

An issue discovered in GitLab CE/EE affecting all versions from 16.11 prior to 17.6.5, 17.7 prior to 17.7.4, and 17.8 prior to 17.8.2 meant that long-lived connections in ActionCable potentially allowed revoked Personal Access Tokens access to streaming results.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/#vector=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:N), 4.2).
It is now mitigated in the latest release and is assigned [CVE-2025-1198](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-1198).

This vulnerability has been discovered internally by a GitLab team member [Dylan Griffith](https://gitlab.com/DylanGriffith).


### A custom permission may allow overriding Repository settings

An improper access control vulnerability in GitLab EE affecting all versions from 15.7 prior to 17.6.5, 17.7 prior to 17.7.4, and 17.8 prior to 17.8.2 allows a user with a custom permission to view contents of a repository even if that access is not authorized.
This is a low severity issue ([`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:L/I:N/A:N), 2.7).
It is now mitigated in the latest release and is assigned [CVE-2025-1042](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2025-1042).

Thanks [mateuszek](https://hackerone.com/mateuszek) for reporting this vulnerability through our HackerOne bug bounty program.


### Mattermost Security Updates January 22, 2025

Mattermost has been updated to versions 10.2.3, which contains several patches and security fixes.


## Bug fixes


### 17.8.2

* [Fix Workhorse failing on 64-bit unaligned access on Raspberry Pi 32-bit](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/178514)
* [[Backport] Fixed css bug for command palette file names too long](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/178575)
* [Merge branch 'fix-environment-check-user-creation' into '17-8-stable-ee'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/178596)
* [17.8: Ensure user external attribute is preserved and not set to nil](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/178452)
* [Backporting the bug: Remove feature flag for multiple approvals](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/178961)
* [Merge branch 'dattang/do-not-allow-release-environment-to-fail' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179349)
* [Fix Approval widget for project merge request settings](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179180)
* [Enable ai tracking without move_ai_tracking_to_instrumentation_layer flag](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179527)
* [Add import_vulnerabilities feature flag](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179948)
* [Update build-gdk-image version](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/180252)
* [Backport: Zoekt code search always performs regex search](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/180216)
* [Fix storing incorrect policy index in scan_result_policies](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/180274)
* [[Backport] Only check pending migrations if indexing enabled](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/180206)
* [Updating Duo functionality note](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/180306)
* [[backport] Fix command palette keybindings propagation](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/180237)
* [Backport into 17.8: Decrease log level of false error](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/180553)
* [[Backport] Fix info and list_pending_migration rake tasks if search cluster unreachable](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/180783)

### 17.7.4

* [17.7: Ensure user external attribute is preserved and not set to nil](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/178457)
* [Merge branch 'fix-environment-check-user-creation' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179058)
* [Merge branch 'dattang/do-not-allow-release-environment-to-fail' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179350)
* [Fixes typo on profiles_controller_spec](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179735)
* [Add import_vulnerabilities feature flag](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179933)
* [Updating Duo functionality note](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/180305)
* [Backport into 17.7: Decrease log level of false error](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/180574)

### 17.6.5

* [Backport internal release support to 17.6](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2199)
* [17.6: Ensure user external attribute is preserved and not set to nil](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/178458)
* [Merge branch 'dattang/do-not-allow-release-environment-to-fail' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179351)
* [Fixes typo on profiles_controller_spec](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/179736)
* [Backport internal release support to 17.6](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/8137)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
