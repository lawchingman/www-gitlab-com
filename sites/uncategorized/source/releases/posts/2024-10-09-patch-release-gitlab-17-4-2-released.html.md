---
title: "GitLab Critical Patch Release: 17.4.2, 17.3.5, 17.2.9"
categories: releases
author: Nikhil George
author_gitlab: ngeorge1
author_twitter: gitlab
description: "Learn more about GitLab Critical Patch Release: 17.4.2, 17.3.5, 17.2.9 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/10/09/patch-release-gitlab-17-4-2-released/'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.4.2, 17.3.5, 17.2.9 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all self-managed GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version. **GitLab Dedicated customers do not need to take action.**

GitLab releases fixes for vulnerabilities in patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are committed to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [Run pipelines on arbitrary branches](#run-pipelines-on-arbitrary-branches) | Critical |
| [An attacker can impersonate arbitrary user](#an-attacker-can-impersonate-arbitrary-user) | High |
| [SSRF in Analytics Dashboard](#ssrf-in-analytics-dashboard) | High |
| [Viewing diffs of MR with conflicts can be slow](#viewing-diffs-of-mr-with-conflicts-can-be-slow) | High |
| [HTMLi in  OAuth page](#htmli-in--oauth-page) | High |
| [Deploy Keys can push changes to an archived repository](#deploy-keys-can-push-changes-to-an-archived-repository) | Medium |
| [Guests can disclose project templates](#guests-can-disclose-project-templates) | Medium |
| [GitLab instance version disclosed to unauthorized users](#gitlab-instance-version-disclosed-to-unauthorized-users) | Low |

### Run pipelines on arbitrary branches

An issue was discovered in GitLab EE affecting all versions starting from 12.5 prior to 17.2.9, starting from 17.3, prior to 17.3.5, and starting from 17.4 prior to 17.4.2, which allows running pipelines on arbitrary branches.
This is a critical severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:N), 9.6).
It is now mitigated in the latest release and is assigned [CVE-2024-9164](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-9164).

Thanks [pwnie](https://hackerone.com/pwnie) for reporting this vulnerability through our HackerOne bug bounty program.


### An attacker can impersonate arbitrary user

An issue was discovered in GitLab CE/EE affecting all versions starting from 11.6 prior to 17.2.9, starting from 17.3 prior to 17.3.5, and starting from 17.4 prior to 17.4.2, which allows an attacker to trigger a pipeline as another user under certain circumstances.
This is a high severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:N), 8.2).
It is now mitigated in the latest release and is assigned [CVE-2024-8970](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8970).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.


### SSRF in Analytics Dashboard

An issue has been discovered in GitLab EE affecting all versions starting from 15.10 prior to 17.2.9, from 17.3 prior to 17.3.5, and from 17.4 prior to 17.4.2. Instances with Product Analytics Dashboard configured and enabled could be vulnerable to SSRF attacks.
This is a high severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:N), 8.2).
It is now mitigated in the latest release and is assigned [CVE-2024-8977](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8977).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Viewing diffs of MR with conflicts can be slow

An issue was discovered in GitLab CE/EE affecting all versions starting from 13.6 prior to 17.2.9, starting from 17.3 prior to 17.3.5, and starting from 17.4 prior to 17.4.2, were viewing diffs of MR with conflicts can be slow. This is a high severity issue ([`AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H), 7.5). It is now mitigated in the latest release and is assigned [CVE-2024-9631](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-9631).

Thanks [a92847865](https://hackerone.com/a92847865) for reporting this vulnerability through our HackerOne bug bounty program.


### HTMLi in OAuth page

A cross-site scripting issue has been discovered in GitLab affecting all versions starting from 17.1 prior 17.2.9, starting from 17.3 prior to 17.3.5, and starting from 17.4 prior to 17.4.2. When authorising a new application, it can be made to render as HTML under specific circumstances.
This is a high severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:H/A:N), 7.3).
It is now mitigated in the latest release and is assigned [CVE-2024-6530](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6530).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


###  Deploy Keys can push changes to an archived repository

An issue was discovered in GitLab CE/EE affecting all versions starting from 8.16 prior to 17.2.9, starting from 17.3 prior to 17.3.5, and starting from 17.4 prior to 17.4.2, which allows deploy keys to push to an archived repository. This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:N/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:N/I:H/A:N), 4.9).
It is now mitigated in the latest release and is assigned [CVE-2024-9623](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-9623).

Thanks [stevenorman](https://gitlab.com/stevenorman) for reporting this vulnerability.

### Guests can disclose project templates

An issue has been discovered discovered in GitLab EE/CE affecting all versions starting from 11.4 before 17.2.9, all versions starting from 17.3 before 17.3.5, all versions starting from 17.4 before 17.4.2 It was possible for guest users to disclose project templates using the API.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:N/A:N), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-5005](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-5005).

Thanks [js_noob](https://hackerone.com/js_noob) for reporting this vulnerability through our HackerOne bug bounty program.


### GitLab instance version disclosed to unauthorized users

An issue has been discovered in GitLab EE affecting all versions starting from 16.6 prior to 17.2.9, from 17.3 prior to 17.3.5, and from 17.4 prior to 17.4.2. It was possible for an unauthenticated attacker to determine the GitLab version number for a GitLab instance.
This is a low severity issue ([`CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:L/I:N/A:N), 3.7).
It is now mitigated in the latest release and is assigned [CVE-2024-9596](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-9596).

This issue was discovered internally by GitLab team member [Paul Gascou-Vaillancourt](https://gitlab.com/pgascouvaillancourt).


## Bug fixes


### 17.4.2

* [Backport: fix: Specify an absolute directory for SCHEMA_VERSIONS_DIR to 17-4-stable](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2041)
* [Backport grpc-go v1.67.1 upgrade to 17.4](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/7323)
* [Update expected vulnerability in enable_advanced_sast_spec.rb](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/167033)
* [Skip multi-version upgrade job for stable branch MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166877)
* [Backport 17.4 Fix label filter by name for search](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168099)
* [Restrict duo pro assignment email to duo pro for sm](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168238)
* [Drop project_id not null constraint ci_deleted_objects](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168156)
* [[Backport] Go-get: fix 401 error for unauthenticated requests](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/167937)

### 17.3.5

* [Backport: fix: Specify an absolute directory for SCHEMA_VERSIONS_DIR to 17-3-stable](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2042)
* [Backport: fix: Allow non-root user to run the bundle-certificates script 17.3](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/2045)
* [Skip multi-version upgrade job for stable branch MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166881)
* [Ensure restricted visibility levels is an array - 17.3 backport](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168015)

### 17.2.9

* [Skip multi-version upgrade job for stable branch MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/166883)
* [Ensure restricted visibility levels is an array - 17.2 backport](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/168016)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
