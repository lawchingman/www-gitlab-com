## Basic setup

1. [ ] Decide which project you would like to become a maintainer of.
1. [ ] Change this issue title to include your name and project:
    `Trainee Maintainer: [Your Name] ([Project])`.
1. [ ] Read the [code review guidelines](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/).
1. [ ] Understand [how to become a maintainer](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer).
1. [ ] Add yourself as a trainee maintainer in the [team database](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md).
1. [ ] Mention a current maintainer of the project you chose to potentially act as your Maintainer Mentor during the traineeship.
1. [ ] Mention your manager in this issue for awareness.
1. [ ] Shadow a maintainer while they review an MR. This will allow you to get insight into the thought processes involved. (You may want to do this multiple times)
1. [ ] Have a maintainer shadow _you_ while you review an MR _as if you were a maintainer_ . Ideally, this would be with a different maintainer to the above, so you can get different insights. (This is also worth doing a few times)

## Working towards becoming a maintainer

This is not a checklist, but guidelines that will help you become a maintainer.
Remember that there is no specific timeline on this, and that you should work
together with your manager and current maintainers.

It is up to you to ensure that you are getting enough MRs to review. If you are
not receiving enough MRs to advance in your training, be proactive and seek out
opportunities for review. Maintainers are available to help guide you.

After each MR is merged or closed, add a discussion to this issue using this
template:

```markdown
### (Merge request title): (Merge request URL)

During review:
- (List anything of note, or a quick summary. "I suggested/identified/noted...")

Post-review:
- (List anything of note, or a quick summary. "I missed..." or "Merged as-is")

(@support-maintainer-username) please add feedback, and compare this review to
the average maintainer review.
```

**Warning:** For security MRs, no discussion should take place in the issue (even if using an [internal note](https://docs.gitlab.com/ee/user/discussions/#add-an-internal-note)), as this might reveal security issue details.
Instead, if you want to discuss the MR, you may create a discussion in the original MR, and link to it from a discussion in the trainee maintainer issue.

## When you're ready to make it official

When reviews have accumulated, and recent reviews consistently fulfill maintainer
responsibilities:

1. [ ] Create a merge request updating [your team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/team_database.md) using [one of available merge request templates](
https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/.gitlab/merge_request_templates?ref_type=heads) proposing yourself as a maintainer for the relevant application.
1. [ ] Follow the instructions in the merge request template

/label ~"trainee maintainer"
